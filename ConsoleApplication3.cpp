// ConsoleApplication3.cpp: Определяет точку входа для приложения.
#include<iostream>
#include <conio.h>
using namespace std;

//11. Среди множества точек (заданного массивом) в трехмерном векторном
// пространстве, найти пару точек наиболее удаленных друг от друга. Вычисление
// расстояния между точками оформить в виде отдельной подпрограммы.
const int the_size = 7;
class exBadIndex {};
struct Point
{
	double x;
	double y;
	double z;
};
Point* AddPoint(double theX, double theY, double theZ)
{
	__asm {
		mov eax, size Point
		push eax
		call dword ptr malloc
		cmp eax, 0
		je NO_MEMORY
		add esp, 4
		finit

		fld theX
		fstp qword ptr[eax]
		fld theY
		add eax, 8
		fstp qword ptr[eax]
		fld theZ
		add eax, 8
		fstp qword ptr[eax]
		sub eax, 16
		NO_MEMORY:
	}
}

void DeletePoint(Point** thePoint)
{
	__asm {
		mov eax, thePoint
		push eax
		mov ebx, [eax]
		push ebx
		call dword ptr free
		pop ebx
		pop eax
		mov ecx, 0
		mov[eax], ecx
	}
}
void PrintPoint(Point* thePoint)
{
	char no_p[] = "There is no point! \n";
	char p[] = "The point: %lf, %lf, %lf. \n";
	__asm {
		mov eax, thePoint
		cmp eax, 0
		je NO_POINT
		finit
		sub esp, 16

		add eax, 16
		fld qword ptr[eax]
		fstp qword ptr[esp + 16]

		sub eax, 8
		fld qword ptr[eax]
		fstp qword ptr[esp + 8]

		sub eax, 8
		fld qword ptr[eax]
		fstp qword ptr[esp]

		lea eax, p
		push eax
		call dword ptr printf
		add esp, 20
		jmp END
		NO_POINT :
		lea eax, no_p
			push eax
			call dword ptr printf
			add esp, 4
			END :
	}
}
double TheDistance(Point * FPoint, Point * SPoint)
{
	_asm {
		mov eax, FPoint
		cmp eax, 0
		je EMPTY_POINTER
		mov ebx, SPoint
		cmp ebx, 0
		je EMPTY_POINTER
		//если указатели на обе точки не пустые
		finit

		fld qword ptr[eax]
		fsub qword ptr[ebx]
		fld st(0)
		fmulp st(1), st(0)

		fld qword ptr[eax + 8]
		fsub qword ptr[ebx + 8]
		fld st(0)
		fmulp st(1), st(0)

		fld qword ptr[eax + 16]
		fsub qword ptr[ebx + 16]
		fld st(0)
		fmulp st(1), st(0)

		faddp st(1), st(0)
		faddp st(1), st(0)
		fsqrt

		jmp END
		EMPTY_POINTER :
		fldz
			END :
	}
}
double Find_the_longest_distance(Point **theArray, int &theFirstPoint, int &theSecondPoint)
{
	Point* p = 0;
	int f, s;
	double max = 0;
	_asm {

		cmp theArray, 0
		je EMPTY
		mov s, 0 // вторая точка
		mov f, 0 // место текущей точки, от которой до остальных справа считаем расстояние
		finit
		CYCLE_FIND_FIRST_POINT :
		mov esi, f
			inc esi
			cmp esi, the_size
			je END_LINE
			dec esi

			mov ecx, theArray
			mov ebx, [ecx + esi * 4]
			cmp ebx, 0
			je GO_TO_NEXT_POINT

			mov edi, f
			inc edi
			mov s, edi

			CYCLE_FIND_SECOND_POINT :

		mov esi, f
			mov ecx, theArray
			mov edi, s
			cmp edi, the_size
			je GO_TO_NEXT_POINT
			mov edx, [ecx + edi * 4]
			cmp edx, 0
			je NEXT_POINT

			mov ebx, [ecx + esi * 4]
			// находим расстояние между ними : в ebx - первая точка, в edx - вторая (указатели)

			push edx
			push ebx
			call dword ptr TheDistance
			add esp, 8
			fcom max
			fstsw ax
			sahf
			jc LESS
			// оставить theDistance
			mov esi, theFirstPoint
			mov edi, f
			mov[esi], edi

			mov esi, theSecondPoint
			mov edi, s
			mov[esi], edi

			//положить в max st(0)
			fst max
			jmp FIN
			LESS :
		//оставляем как есть т.к. max больше
	FIN:

	NEXT_POINT:
		mov esi, s
			inc esi
			mov s, esi

			jmp CYCLE_FIND_SECOND_POINT
			GO_TO_NEXT_POINT :
		mov esi, f
			inc esi
			mov f, esi //f++

			mov edi, f
			inc edi
			mov s, edi
			jmp CYCLE_FIND_FIRST_POINT
			END_LINE :
		fld max
			jmp END

			EMPTY :
		fldz
			mov esi, theFirstPoint
			mov[esi], 0
			mov edi, theSecondPoint
			mov[edi], 0
			END :
	}
}
void main()
{
	char key = 49;
	Point *Array[the_size] = { 0 };
	while (key != 48)
	{
		system("cls");
		cout<<"0. Exit." << endl;
		cout<<"1. Add a point." << endl;
		cout << "2. Delete a point." << endl;
		cout << "3. Print a point." << endl;
		cout << "4. Print all points." << endl;
		cout << "5. Get the longest distance in set of points."<<endl;
		key = _getch();
		if ((key<48) || (key>54))
		{
			cout << "Error! Wrong button!"; _getch();
		}
		else
		{
			switch (key)
			{
			case '0': {cout << "Bye!" << endl; }
					  break;
			case '1':
			{
				try {
					int place = 0;
					double tX = 0, tY = 0, tZ = 0;
					cout << "Key in place in Array" << endl;
					cin>>place;
					if (place<0 || place>the_size) throw exBadIndex();
					cout<<"Key the first coordinate"<<endl;
					cin.ignore();
					cin>>tX;
					cout<<"Key the second coordinate"<<endl;
					cin>>tY;
					cout<<"Key the third coordinate"<<endl;
					cin>>tZ;
					Array[place] = AddPoint(tX, tY, tZ);
				}
				catch (exBadIndex)
				{
					cout<<"bad index";
					_getch();
				}
			}
			break;
			case '2':
			{
				try {
					int place = 0;
					cout<<"Key in place in Array"<<endl;
					cin>>place;
					if
						(place<0 || place>the_size) throw exBadIndex();
					DeletePoint(&Array[place]);
					cout<<"Done!"<<endl;
					_getch();
				}
				catch (exBadIndex)
				{
					cout<<"bad index";
					_getch();
				}
			}
			break;
			case '3':
			{
				try {
					int place = 0;
					cout<<"Key in place in Array"<<endl;
					cin>>place;
					if (place<0 || place>the_size) throw exBadIndex();
					PrintPoint(Array[place]);
					_getch();
				}
				catch (exBadIndex)
				{
					cout<<"bad index";
					_getch();
				}
			}
			break;
			case '4':
			{
				for (int i = 0; i<the_size; ++i)
					PrintPoint(Array[i]);
				_getch();
			}
			break;
			case '5':
			{
				
				double max = 0;
				int FirstPoint = 0, SecondPoint = 0;
				max = Find_the_longest_distance(Array, FirstPoint, SecondPoint);
				cout<<"The longest distance is: "<<max<<"\n The points are: \n";
				PrintPoint(Array[FirstPoint]);
				cout<<"\n";
				PrintPoint(Array[SecondPoint]);
				cout<<"\n";
				_getch();

			}

			default: cout<<"Done!"<<endl;
				break;
			}
		}
	}
}