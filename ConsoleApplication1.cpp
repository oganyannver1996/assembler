// ConsoleApplication1.cpp : Defines the entry point for the console application.
// Программа из массива струтктур удаляет все повторяющиеся элементы и выводит их на экран 
// выполнил Оганян Нвер
#include "stdafx.h" 
#include <stdio.h> 
#include <stdlib.h> 
#include <conio.h> 
#include<iostream> 
int SIZEARRAY = 5;

struct BookCard //Книжная карточка 
{
	char Title[30];
	char Author[30];
};

BookCard* createBookCard(char* title, char* author) {
	BookCard *bc = 0;
	__asm {
		mov eax, 60;
		push eax;
		call malloc
			add esp, 4
			mov edx, eax
			mov ebx, title
			CYCLE :
		cmp byte ptr[ebx], 0
			je FILL_AUTHOR
			mov ch, [ebx]
			mov[eax], ch
			inc eax
			inc ebx
			jmp CYCLE
			FILL_AUTHOR :
		mov byte ptr[eax], 0
			mov eax, edx
			add eax, 30
			xor ebx, ebx
			mov ebx, author
			CYCLE_TWO :
		cmp[ebx], 0
			je FIN
			mov ch, [ebx]
			mov[eax], ch
			inc eax
			inc ebx
			jmp CYCLE_TWO


			FIN :
		mov[eax], 0
			mov bc, edx
	}
	return bc;

}

void deleteBookCard(BookCard* bc) {
	__asm {
		mov ebx, bc
		cmp[ebx], 0
		je FIN
		push ebx
		call free
		add esp, 4
		mov[ebx], 0
		mov[ebx + 30], 0
		xor ebx, ebx
		FIN :

	}
	SIZEARRAY--;
}

void printBookCard(BookCard* bc) {
	char format[] = "Title: %s, Author: %s \n";
	__asm {
		mov eax, bc
		cmp eax, 0
		je FIN
		cmp[eax], 0
		je FIN
		add eax, 30
		push eax
		sub eax, 30
		push eax
		lea eax, format
		push eax
		call printf
		add esp, 12
		FIN :
	}
}

void fillBookCards(BookCard* bookCards[]) {
	BookCard* bc = 0;
	char title[30];
	char author[30];
	int count = 0;
	//char* format2 = "Введите название статьи, имя автора\n" ; 
	__asm {
		mov ebx, bookCards
		mov edx, ebx
		xor ecx, ecx
		CYCLE :
		cmp ecx, SIZEARRAY;
		je FIN
			mov count, ecx
			push 30
			lea eax, title
			push eax
			call gets_s
			add esp, 8
			push 30
			lea eax, author
			push eax
			call gets_s
			add esp, 8
			lea eax, author
			push eax
			lea eax, title
			push eax
			call createBookCard
			add esp, 8
			mov[ebx], eax
			mov ecx, count
			inc ecx
			add ebx, 4
			jmp CYCLE
			FIN :
	}
}

void printBookCards(BookCard* bookCards[]) {
	for (int i = 0; i < SIZEARRAY; i++) {
		printBookCard(bookCards[i]);
	}
}

void task(BookCard* bookCards[]) {
	int count = SIZEARRAY - 1;
	int count1 = 0;
	int count2 = 0;
	int temp = 0;
	int c = 1;
	__asm {
		lea eax, bookCards;
		mov ebx, [eax];
		mov eax, ebx;
		xor ecx, ecx;
		mov edx, [eax + 4 * ecx];
		mov ecx, c;
		inc ecx;
		mov c, ecx;
		sub ecx, 1;
	CYCLE:
		cmp ecx, 5;
		je NEXT;
		xor edi, edi;
		xor esi, esi;
		mov count2, ecx;
		; cmp[eax + 4 * ecx], 0
		; je NEXT
		mov ebx, [eax + 4 * ecx];
		xor ecx, ecx;
	CHECK_TITLE:
		mov ch, [edx][edi];
		mov cl, [ebx][edi];
		cmp ch, cl;
		jne PRE_CYCLE;
		cmp ch, 0;
		je CHECK_AUTHOR;
		inc edi;
		jmp CHECK_TITLE;
	CHECK_AUTHOR:
		mov ch, [edx + 30][esi];
		mov cl, [ebx + 30][esi];
		cmp ch, cl;
		jne PRE_CYCLE;
		cmp ch, 0;
		je CLEAR;
		inc esi;
		jmp CHECK_AUTHOR;

	CLEAR:
		push eax;
		push edx;
		push ebx;
		call deleteBookCard;
		add esp, 4;
		pop edx;
		pop eax;
		jmp PRE_CYCLE;

	NEXT:
		jmp FIN
			PRE_CYCLE :
		mov ecx, count2;
		inc ecx;
		jmp CYCLE;

	FIN:
	}

}

int main()
{
	setlocale(LC_ALL, "rus");
	BookCard* bookCards[5];
	fillBookCards(bookCards);
	task(bookCards);
	printBookCards(bookCards);
	system("pause");
	return 0;
}